# Menu trail by node

## Overview

Menu trail by node allows to set the active-trail depending on the menu item you choose per content type.

Under some circumstances, site builders needs to display some menu items in pages where the content being displayed is 
not part of the menu tree and therefore we don't have any menu item as part of the active trail.

This module allows you to choose a menu-item from any menu defined in your site by content type. Then when you visit a 
node of a certain content type, the active trail is set based on the menu-item you have chosen for that content type.

## Configuration
To configure the menu_trail_by_node modules just follow the following steps:

* Enable the module like any other Drupal module.
* Visit the edit page of any content type in your project.
* Under the tab "Menu trail by node settings" choose the menu from which you'd like to select 
  a menu item to active the trails.
* Choose a menu from the selected menu to active the trails when any user visit a node of this content type.   

## Installation
Just enable it as any other Drupal module.
