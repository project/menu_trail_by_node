<?php

namespace Drupal\menu_trail_by_node;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Overrides the class for the menu link tree.
 */
class MenuTrailByNodeServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('menu.active_trail');
    $definition->setClass('Drupal\menu_trail_by_node\MenuTrailByNodeActiveTrail');
    $definition->getArguments();
    $definition->addArgument(new Reference('entity_type.manager'));
  }

}
