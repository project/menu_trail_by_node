<?php

namespace Drupal\menu_trail_by_node;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Menu\MenuActiveTrail;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Overrides the class for the file entity normalizer from HAL.
 */
class MenuTrailByNodeActiveTrail extends MenuActiveTrail {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\menu_trail_by_node\MenuTrailByNodeActiveTrail object.
   *
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager
   *   The menu link plugin manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A route match object for finding the active link.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock backend.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type_manager service.
   */
  public function __construct(MenuLinkManagerInterface $menu_link_manager, RouteMatchInterface $route_match, CacheBackendInterface $cache, LockBackendInterface $lock, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($menu_link_manager, $route_match, $cache, $lock);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  protected function doGetActiveTrailIds($menu_name) {
    $active_trail = parent::doGetActiveTrailIds($menu_name);

    if ($this->isNodePath()) {
      $node_id = $this->routeMatch->getRawParameters()->get('node');
      $config = $this->getActiveTrailConfig($node_id);

      // Check if the menu is configured for this content type:
      if (isset($config['menu_active_trail_options']) && in_array($menu_name, $config['menu_active_trail_options'])) {
        $active_trail = ['' => ''];
        $menu_item_id = $this->getMenuPluginId($config['menu_active_trail_item']);

        if (!empty($menu_item_id)) {
          // We set to active, not only the menu but its parents.
          if ($parents = $this->menuLinkManager->getParentIds($menu_item_id)) {
            $active_trail = $parents + $active_trail;
          }

        }
      }
    }

    return $active_trail;
  }

  /**
   * Checks whether the current route is the node canonical or not.
   *
   * @return bool
   *   TRUE if the current route is the entity.node.canonical.
   */
  protected function isNodePath() {
    $is_node_path = FALSE;

    $route_name = $this->routeMatch->getRouteName();
    if (!empty($route_name) && $route_name == 'entity.node.canonical') {
      return TRUE;
    }

    return $is_node_path;
  }

  /**
   * Retrieves the menu pluignID given a complete menu id definition.
   *
   * @param string $menu_item_to_active_conf
   *   The complete menu id definition.
   *
   * @return string
   *   The menu item plugin id.
   */
  private function getMenuPluginId(string $menu_item_to_active_conf) {
    $splits = explode(':', $menu_item_to_active_conf);
    return $splits[1] . ':' . $splits[2];
  }

  /**
   * Retrieves the active trail config related with the given $node_id.
   *
   * @param int $node_id
   *   The node id used to get the content type definition and therefore its
   *   config related with.
   *
   * @return array|mixed
   *   The menu_trail_by_node active trail config.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getActiveTrailConfig(int $node_id) {
    $storage = $this->entityTypeManager->getStorage('node');
    $node = $storage->load($node_id);
    /** @var \Drupal\node\Entity\NodeType $node_type */
    $node_type = $node->type->entity;
    return $node_type->getThirdPartySettings('menu_trail_by_node');
  }

}
